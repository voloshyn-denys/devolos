
/*Parallax for logo */
var scene = document.getElementById('scene');
var parallax = new Parallax(scene);

$(document).ready(function() {

    /*Pagpiling config*/
    $('#pagepiling').pagepiling({
        anchors: ['section1', 'section2', 'section3', 'section4'],
        sectionsColor: ['white', 'white', 'white', 'white']
    });

    /*Lightbox for portfolio*/
    $(function(){
        $('a').imageLightbox();
    });
    /*Row for next section*/
    function moveRow(){
        var ths = '.move-rows';
        $(ths).animate({"top": "+=15px"}, "slow");
        $(ths).animate({"top": "-=15px"}, "slow");
        setTimeout(moveRow, 1);
    };

    moveRow();
});






