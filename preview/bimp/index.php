<!DOCTYPE html>
<html lang="en" class="bg-html-main">
<head>
    <meta charset="UTF-8">
    <title>Bimp</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="shortcut icon" href="img/favicon.png" type="image/png">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
</head>
<body class="main-wrap">
        <header class="main_header">
           <div class="container">
            <nav class="main_header_categoties">
                <ul class="main_header_categoties_menu">
                    <li class="main_header_categoties_menu_item">
                        <a href="index.php" class="main_header_categoties_menu_item_link transition"><img class="main_header_categoties_menu_item_img" src="img/main_bimp_gray.png" alt="" width="37px">Main page</a>
                    </li>                   
                    <li class="main_header_categoties_menu_item">
                        <a href="love.html" class="main_header_categoties_menu_item_link transition"><img class="main_header_categoties_menu_item_img" src="img/1_love_main.png" alt="" width="50" height="44">love relations</a>
                    </li>
                    <li class="main_header_categoties_menu_item">
                        <a href="friends.html" class="main_header_categoties_menu_item_link"><img class="main_header_categoties_menu_item_img" src="img/2_friend_main.png " alt="" width="48" height="44">friend relations</a> 
                    </li>
                    <li class="main_header_categoties_menu_item">
                        <a href="intimate.html" class="main_header_categoties_menu_item_link"><img class="main_header_categoties_menu_item_img" src="img/3_intimate_main.png" alt="" width="51" height="43">intimate elations</a> 
                    </li>
                    <li class="main_header_categoties_menu_item">
                        <a href="auto.html" class="main_header_categoties_menu_item_link"><img class="main_header_categoties_menu_item_img" src="img/4_auto_main.png" alt="" width="57" height="43">fellow travelers</a>  
                    </li>
                    <li class="main_header_categoties_menu_item">
                        <a href="adventures.html" class="main_header_categoties_menu_item_link"><img class="main_header_categoties_menu_item_img" src="img/5_adv_main.png" alt="" width="59" height="60">adventures</a> 
                    </li>
                    <li class="main_header_categoties_menu_item">
                        <a href="dinner.html" class="main_header_categoties_menu_item_link"><img class="main_header_categoties_menu_item_img" src="img/6_dinner_main.png" alt="" width="56" height="43">companions on diner</a> 
                    </li>
                </ul>
            </nav>
            <div class="main_header_categoties_caption">
                <span class="main_header_categoties_caption_text">click on any of categories</span>
            </div>
            
            <div class="main_header_call-to-action">
                <span class="main_header_call-to-action_first-text">Hello!</span>
                <p class="main_header_call-to-action_second-text">here you can see all the catogories and also watch  mini-tour of all of them</p>
            </div>
            <div class="main_header_social">
                <ul class="main_header_social_list">
                    <li class="main_header_social_list_item">
                        <a href="" class="main_header_social_list_item_link icon_vk">Вконтакте</a>
                    </li>
                    <li class="main_header_social_list_item">
                        <a href="" class="main_header_social_list_item_link icon_google">Google+</a>
                    </li>
                    <li class="main_header_social_list_item">
                        <a href="" class="main_header_social_list_item_link icon_tw">Twitter</a>
                    </li>
                    <li class="main_header_social_list_item">
                        <a href="" class="main_header_social_list_item_link icon_fb">Facebook</a>
                    </li>
                    <li class="main_header_social_list_item">
                        <a href="" class="main_header_social_list_item_link icon_od">Одноклассники</a>
                    </li>
                </ul>
            </div>
            <div class="main_header_lang">
                <a href="" class="main_header_lang_link lang-active">English</a>
                <a href="ru/index.php" class="main_header_lang_link">Русский</a>
            </div>
        </div>
        </header>                                        
    <div class="main container">
        <main class="main_content">
           <div class="main_content_promo">
               <p class="main_content_promo_pre-title"> We are pleased to introduce to you<br> <span class="main_content_promo_pre-title_second-text">The first <span class="green">useful</span> social network</span></p>
               <h1 class="main_content_promo_title">THE Bimp</h1>
               <h2 class="main_content_promo_post-title">appreciate every moment...</h2>
           </div>
           <div class="main_content_subscribe">
               <h2 class="main_content_subscribe_title">you can just trust us that the bimp will be a social network of next generation  and subscribe for us for your invitation of beta test</h2>
               
               <form class="main_content_subscribe_form" action="" method="post">
                   <i class="icon-mail"></i>
                   <input class="main_content_subscribe_form_textarea" type="email" name="email">
                   <input class="main_content_subscribe_form_button" type="submit" value="subscribe" name="btn">
               </form>

<?php 
$s = implode(";", $_POST); 
$f = fopen("email_eng.txt","a"); 
flock($f, LOCK_EX); 
fputs($f, $s."\n"); 
flock($f, LOCK_UN); 
fclose($f); 
if($s){
    echo '
    <div id="box_message" class="container-message">
        <div id="close" class="container-message_close">x</div>
        <p>Вы успешно подписались!'.'<br>'.' вас оповестят когда выйдет тестовая версия приложения</p>
    </div>
    ';
}
?>                
               
               
               <p class="main_content_subscribe_notification">Your email will never be used anywhere, except sending to you an invitation of beta-test of app</p>
               
           </div>
           <div class="main_content_mobile">
              <div class="main_content_mobile_timer">
                  <script src="http://megatimer.ru/s/4940909d03fea68fe7ab28cf5eaf4ccf.js"></script>
              </div>
              
              <a href="#" class="main_content_mobile_app-download">Download the alpha version<br>
              (only check-in)</a>
              
           </div>             
    <div class="switcher_nav">
       <a class="switcher switcher_nav_right transition" href="love.html">
       </a>
    </div>             
  </main>       

</div>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="js/custom.js"> </script>
<!--close message-->
<script>
    $('#close').click(function(){
        $('#box_message').hide('slow');
    });
</script>
<!-- HelloPreload http://hello-site.ru/preloader/ -->
<style type="text/css">#hellopreloader>p{display:none;}#hellopreloader_preload{display: block;position: fixed;z-index: 99999;top: 0;left: 0;width: 100%;height: 100%;min-width: 1000px;background: #3498DB url(http://hello-site.ru//main/images/preloads/oval.svg) center center no-repeat;background-size:41px;}</style>
<div id="hellopreloader"><div id="hellopreloader_preload"></div><p><a href="http://hello-site.ru">Hello-Site.ru. Бесплатный конструктор сайтов.</a></p></div>
<script type="text/javascript">var hellopreloader = document.getElementById("hellopreloader_preload");function fadeOutnojquery(el){el.style.opacity = 1;var interhellopreloader = setInterval(function(){el.style.opacity = el.style.opacity - 0.05;if (el.style.opacity <=0.05){ clearInterval(interhellopreloader);hellopreloader.style.display = "none";}},16);}window.onload = function(){setTimeout(function(){fadeOutnojquery(hellopreloader);},1000);};</script>
<!-- HelloPreload http://hello-site.ru/preloader/ -->
</body>
</html>