$(document).ready(function() {
	
	$("body").css("display", "none");

    $("body").fadeIn(300);
    
	$("a.transition").click(function(event){
		event.preventDefault();
		linkLocation = this.href;
		$("body").fadeOut(150, redirectPage);		
	});
		
	function redirectPage() {
		window.location = linkLocation;
	}
	
});
