<!DOCTYPE html>
<html lang="ru" class="bg-html-news">
<head>
    <meta charset="UTF-8">
    <title>Bimp - Новости и Обратная Связь</title>
    <link rel="stylesheet" href="../css/main.css">
    <link rel="shortcut icon" href="../img/favicon.png" type="image/png">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
</head>
<body class="news-wrap">  
    <div class="news container">
        <header class="news_header">
            <nav class="news_header_categoties">
                <ul class="news_header_categoties_menu">
                   <li class="news_header_categoties_menu_item">
                        <a href="index.php" class="news_header_categoties_menu_item_link transition"><img class="news_header_categoties_menu_item_img" src="../img/main_bimp_white.png" alt="" width="37">Main Page</a>
                    </li>
                    <li class="news_header_categoties_menu_item">
                        <a href="love.html" class="news_header_categoties_menu_item_link transition"><img class="news_header_categoties_menu_item_img" src="../img/1_love_white.png" alt="">love relations</a>
                    </li>
                    <li class="news_header_categoties_menu_item">
                        <a href="friends.html" class="news_header_categoties_menu_item_link"><img class="news_header_categoties_menu_item_img" src="../img/2_friend_white.png" alt="">friend relations</a> 
                    </li>
                    <li class="news_header_categoties_menu_item">
                        <a href="intimate.html" class="news_header_categoties_menu_item_link"><img class="news_header_categoties_menu_item_img" src="../img/3_intimate_white.png" alt="">intimate elations</a> 
                    </li>
                    <li class="news_header_categoties_menu_item">
                        <a href="auto.html" class="news_header_categoties_menu_item_link"><img class="news_header_categoties_menu_item_img" src="../img/4_auto_white.png" alt="">fellow travelers</a>  
                    </li>
                    <li class="news_header_categoties_menu_item">
                        <a href="news.html" class="news_header_categoties_menu_item_link"><img class="news_header_categoties_menu_item_img" src="../img/5_adv_white.png" alt="">adventures</a>
                    </li>
                    <li class="news_header_categoties_menu_item">
                        <a href="dinner.html" class="news_header_categoties_menu_item_link"><img class="news_header_categoties_menu_item_img" src="../img/6_dinner_white.png" alt="">companions on diner</a> 
                    </li>
                    <li class="news_header_categoties_menu_item">
                        <a href="screens.html" class="news_header_categoties_menu_item_link"><img class="news_header_categoties_menu_item_img" src="../img/7_app_windows_white.png" alt="">app’s windows</a> 
                    </li>
                    <li class="news_header_categoties_menu_item">
                        <a href="news-feedback.php" class="news_header_categoties_menu_item_link menu-active-news"><img class="news_header_categoties_menu_item_img" src="../img/8_news_green.png" alt="">news / feedback</a> 
                    </li>
                </ul>
            </nav>

            <div class="screens_header_lang">
                <a href="../news-feedback.php" class="screens_header_lang_link">English</a>
                <a href="news-feedback.php" class="screens_header_lang_link lang-active">Русский</a>
            </div>

            <div class="screens_header_social">
                <ul class="screens_header_social_list">
                    <li class="screens_header_social_list_item">
                        <a href="" class="screens_header_social_list_item_link icon_vk">Вконтакте</a>
                    </li>
                    <li class="screens_header_social_list_item">
                        <a href="" class="screens_header_social_list_item_link icon_google">Google+</a>
                    </li>
                    <li class="screens_header_social_list_item">
                        <a href="" class="screens_header_social_list_item_link icon_tw">Twitter</a>
                    </li>
                    <li class="screens_header_social_list_item">
                        <a href="" class="screens_header_social_list_item_link icon_fb">Facebook</a>
                    </li>
                    <li class="screens_header_social_list_item">
                        <a href="" class="screens_header_social_list_item_link icon_od">Одноклассники</a>
                    </li>
                </ul>
            </div>

        </header>
        
        <main class="news_content">
           <section class="news_content_news">
           <h2 class="news_content_news_title">NEWS</h2>
           <span class="news_content_news_post-title">stay in touch!</span>
           <div class="news_content_news_articles">
              <?php
                include("edit-news/show_news.php");
              ?>
           </div>

           </section>  
           
           
           <section class="news_content_feedback">
           <h2 class="news_content_feedback_title">feedback</h2>
           <span class="news_content_feedback_post-title">dont hasitate to  write us!</span>
              <div class="news_content_feedback_form">
                   <form class="news_content_feedback_form_block" id="form" action="" method=post>
                        <?php
	                       if($_POST['sendcall']) { 
		                      $name = strip_tags( stripslashes( $_POST['name'] ) );
		                      $email = strip_tags( stripslashes( $_POST['mail'] ) );
                              $message = strip_tags( stripslashes( $_POST['message'] ) );
                        
		                      $total = "<i>Требуется обратная связь!</i><br/>" . "<b>Имя:</b> " . $name . "<br/><b>Телефон:</b> " . $email . "<br/><b>Сообщение:</b> " . $message;

		                      $headers = "From: $name <$email>\n"; 
		                      $headers .= "Content-Type: text/html; charset=utf-8\n"; 

		                      $title = "Заказ обратной связи"; 

		                      if ($name!="" && $email!="" && $message!="") {
			                    mail("fin4friends@gmail.com", $title, $total, $headers);
                                echo "Ваше сообщение отправлено! Хорошего Дня!";
		                      }

	                       } 
                        ?>
         
                       <h3 class="news_content_feedback_form_block_title">your name</h3>
                       <input class="news_content_feedback_form_block_input" type="text" placeholder="your name" name="name" required="required">
                       <h3 class="news_content_feedback_form_block_title">your email</h3>
                       <input class="news_content_feedback_form_block_input" type="email" placeholder="your email" name="mail" required="required">
                       <h3 class="news_content_feedback_form_block_title">your message</h3>
                       <textarea name="message" class="news_content_feedback_form_block_textarea" name="" id="" cols="40" rows="9" required="required"></textarea>
                       
                       <input class="news_content_feedback_form_block_btn" type="submit" value="send" name="sendcall">
                       
                   </form>
              </div>
                        

           </section>                                                       
        </main>
        
    <div class="switcher_nav">
       <a class="switcher switcher_nav_left-white transition" href="screens.html"></a>
    </div>   
        
</div>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="../js/jquery-2.1.4.min.js"></script>
<script src="../js/custom.js"></script>

</body>
</html>