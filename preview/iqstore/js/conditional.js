Modernizr.load([
   {
      test: Modernizr.mq('only all'),
      nope: 'js/respond.min.js'
   },
   {
      test: Modernizr.mq('only screen and (max-width : 555px)'),
      nope: '//code.jquery.com/jquery-1.12.0.min.js',
      complete: function(){

         $(document).ready(function() {
            $('.menu__btn').click(function() {
               $('.menu__navigation').slideToggle(555);
            });//end slide toggle

            $(window).resize(function() {
               if (  $(window).width() > 555 ) {
                  $('.menu__navigation').removeAttr('style');
               }
            });//end resize
         });//end ready

      }
   }
]);